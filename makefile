SHELL:=/bin/bash



export P2_AMI := ami-8c4288f4
export instanceType := p2.xlarge
export deep_learning_security_group := sg-e3837f9f
export forsite_subnets := subnet-8e0069d7
export key_pair_name := oregon

aws:
	rm ~/.aws/config || true
	rm  ~/.aws/credentials || true
	cp .config  ~/.aws/config
	cp .credentials  ~/.aws/credentials

# get_INSTANCE_ID:
# 	aws ec2 describe-instances --filters "Name=instance-type,Values=${instanceType}" --output=json



#####################################################################################################################
#
#			 GETS the Instance Id of the P2 instance as long there is just one - it gets the first one
#
#####################################################################################################################

up:
	${MAKE} start
	@sleep 5
	${MAKE} ssh

get_INSTANCE_ID:
	 $(eval INSTANCE_ID = ${shell aws ec2 describe-instances --filters "Name=instance-type,Values=${instanceType}" --query "Reservations[0].Instances[0].InstanceId"} )

get_INSTANCE_IP:
	 $(eval INSTANCE_IP = ${shell aws ec2 describe-instances --filters "Name=instance-type,Values=${instanceType}" --query "Reservations[0].Instances[0].PublicIpAddress"})


notebook: get_INSTANCE_IP
	@echo http://${INSTANCE_IP}:8888
	open http://localhost:8888

ssh: get_INSTANCE_IP
	ssh -o "StrictHostKeyChecking no" -i ~/.ssh/${key_pair_name}.pem ubuntu@${INSTANCE_IP} -L:8888:localhost:8888

	
stop: get_INSTANCE_ID
	@echo stopping instance
	aws ec2 stop-instances --instance-ids ${INSTANCE_ID}
	@echo please wait...
	aws ec2 wait instance-stopped --instance-ids ${INSTANCE_ID}
	@echo ----------------------------------------------------
	@echo ----------------------------------------------------
	@echo 			INSTANCE STOPPED
	@echo ----------------------------------------------------
	@echo ----------------------------------------------------

start: get_INSTANCE_ID
	@echo starting instance
	aws ec2 start-instances --instance-ids ${INSTANCE_ID}
	@echo please wait...
	@aws ec2 wait instance-running --instance-ids ${INSTANCE_ID}
	@echo ----------------------------------------------------
	@echo ----------------------------------------------------
	@echo 			INSTANCE RUNNING
	@echo ----------------------------------------------------
	@echo ----------------------------------------------------


reboot: get_INSTANCE_ID
	aws ec2 reboot-instances --instance-ids ${INSTANCE_ID}

status: get_INSTANCE_ID
	aws ec2 describe-instances --instance-ids ${INSTANCE_ID} --query "Reservations[0].Instances[0].State.Name"

ip:
	aws ec2 describe-instances --filters "Name=instance-id,Values=${INSTANCE_ID}" --query "Reservations[0].Instances[0].PublicIpAddress"



# launch NEW INSTANCE
launch:
	aws ec2 run-instances \
		--image-id ${P2_AMI} \
		--count 1 \
		--instance-type ${instanceType} \
		--key-name ${key_pair_name} \
		--security-group-ids ${deep_learning_security_group} \
		--subnet-id ${forsite_subnets} \
		--associate-public-ip-address \
		--block-device-mapping "[ { \"DeviceName\": \"/dev/sda1\", \"Ebs\": { \"VolumeSize\": 128, \"VolumeType\": \"gp2\" } } ]"

# DELETE INSTANCE
abandon: get_INSTANCE_ID
	aws ec2 terminate-instances --instance-ids ${INSTANCE_ID}




